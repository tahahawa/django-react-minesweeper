from django.urls import path
from . import views

urlpatterns = [
    # path("", views.MinesweeperListCreate.as_view()), # for debugging
    path("new_game", views.new_game),
    path("get_game/<int:pk>", views.get_game),
    path("reveal_tile/<int:pk>", views.reveal_tile),
    path("toggle_mark_tile/<int:pk>", views.toggle_mark_tile),
]
