from django.db import models
from enum import IntEnum
from picklefield.fields import PickledObjectField
import random


class Cell(IntEnum):
    Empty = 0
    Bomb = 1
    Number = 2
    Marked = 3


"""
Derive pickle object field, and change its serialization,
so that it's JSON readable
"""


class PickledObjectFieldJSON(PickledObjectField):
    def value_to_string(self, obj):
        value = self.value_from_object(obj)
        return value


class Minesweeper(models.Model):
    """2D array of mines and numbers"""
    mine_map = PickledObjectFieldJSON(null=True)

    """Which tiles have been revealed by the player"""
    revealed = PickledObjectFieldJSON(null=True)

    """Tiles the player has marked"""
    marked_locations = PickledObjectFieldJSON(null=True)
    """Number of mines"""
    mines = models.IntegerField(null=True)

    """Locations of mines"""
    mine_locations = PickledObjectField(null=True)

    """Board size"""
    x = models.IntegerField(null=True)
    y = models.IntegerField(null=True)
    """Game status boolean, if complete == true, game over"""
    complete = models.BooleanField(null=True)

    """Player win status"""
    win = models.BooleanField(null=True)

    created_at = models.DateTimeField(auto_now_add=True)

    def toggle_mark_tile(self, x, y):
        if not self.complete:
            """Are we adding or removing a marker?"""
            if [x, y] not in self.marked_locations:
                """Must be adding; have they used up all their markers?"""
                if len(self.marked_locations) <= self.mines:
                    self.marked_locations.append([x, y])
            else:
                # Must be removing
                self.marked_locations.remove([x, y])

            """
            Check if the player has found all the bombs
            If so, reveal all but the marked bombs
            """
            self.marked_locations.sort()
            self.mine_locations.sort()
            if self.marked_locations == self.mine_locations:
                self.win = True
                self.complete = True
                for ax in range(self.x):
                    for ay in range(self.y):
                        if [ax, ay, self.mine_map[ax][ay]] not in self.revealed and [
                            ax,
                            ay,
                        ] not in self.marked_locations:
                            self.revealed.append([ax, ay, self.mine_map[ax][ay]])

    def reveal_tile(self, x, y):
        if not self.complete:
            if [x, y, self.mine_map[x][y]] not in self.revealed:

                """If it's marked, unmark it before reveal"""
                if [x, y] in self.marked_locations:
                    self.marked_locations.remove([x, y])

                # self.revealed.append([x, y, self.mine_map[x][y]])
                self.reveal_tile_neighbours(x, y)

        """If it's a bomb, reveal all and set flags"""
        if self.mine_map[x][y] is Cell.Bomb:
            self.win = False
            self.complete = True
            for ax in range(self.x):
                for ay in range(self.y):
                    if [ax, ay, self.mine_map[ax][ay]] not in self.revealed:
                        self.revealed.append([ax, ay, self.mine_map[ax][ay]])

    """Recursively open empty tiles and 1 deep numbered tiles"""
    def reveal_tile_neighbours(self, x, y):
        if (
            self.mine_map[x][y] is Cell.Empty
            and [x, y, self.mine_map[x][y]] not in self.revealed
        ):
            self.revealed.append([x, y, self.mine_map[x][y]])
            if x + 1 < self.x:
                self.reveal_tile_neighbours(x + 1, y)
            if x - 1 >= 0:
                self.reveal_tile_neighbours(x - 1, y)
            if y + 1 < self.y:
                self.reveal_tile_neighbours(x, y + 1)
            if y - 1 >= 0:
                self.reveal_tile_neighbours(x, y - 1)
            if x + 1 < self.x and y + 1 < self.y:
                self.reveal_tile_neighbours(x + 1, y + 1)
            if x + 1 < self.x and y - 1 >= 0:
                self.reveal_tile_neighbours(x + 1, y - 1)
            if x - 1 >= 0 and y - 1 >= 0:
                self.reveal_tile_neighbours(x - 1, y - 1)
            if x - 1 >= 0 and y + 1 < self.y:
                self.reveal_tile_neighbours(x - 1, y + 1)
        if (
            type(self.mine_map[x][y]) is list
            and [x, y, self.mine_map[x][y]] not in self.revealed
        ):
            self.revealed.append([x, y, self.mine_map[x][y]])

    """Generate a random minesweeper board"""
    def generate(self):
        self.mine_map = []
        self.revealed = []
        self.marked_locations = []
        self.mine_locations = []
        self.complete = False
        self.win = False
        while len(self.mine_locations) < self.mines:
            coord = [random.randrange(self.x), random.randrange(self.y)]
            if coord not in self.mine_locations:
                self.mine_locations.append(coord)

        for x in range(self.x):
            col = []
            for y in range(self.y):
                col.append(Cell.Empty)
            self.mine_map.append(col)

        """Set the numbers around the locations of the bombs"""
        for loc in self.mine_locations:
            x = loc[0]
            y = loc[1]
            if x + 1 < self.x:
                try:
                    self.mine_map[x + 1][y][1] += 1
                except:
                    self.mine_map[x + 1][y] = [Cell.Number, 1]
            if x - 1 >= 0:
                try:
                    self.mine_map[x - 1][y][1] += 1
                except:
                    self.mine_map[x - 1][y] = [Cell.Number, 1]
                    pass
            if y + 1 < self.y:
                try:
                    self.mine_map[x][y + 1][1] += 1
                except:
                    self.mine_map[x][y + 1] = [Cell.Number, 1]
                    pass
            if y - 1 >= 0:
                try:
                    self.mine_map[x][y - 1][1] += 1
                except:
                    self.mine_map[x][y - 1] = [Cell.Number, 1]
                    pass
            if x + 1 < self.x and y + 1 < self.y:
                try:
                    self.mine_map[x + 1][y + 1][1] += 1
                except:
                    self.mine_map[x + 1][y + 1] = [Cell.Number, 1]
                    pass
            if x + 1 < self.x and y - 1 >= 0:
                try:
                    self.mine_map[x + 1][y - 1][1] += 1
                except:
                    self.mine_map[x + 1][y - 1] = [Cell.Number, 1]
                    pass
            if x - 1 >= 0 and y - 1 >= 0:
                try:
                    self.mine_map[x - 1][y - 1][1] += 1
                except:
                    self.mine_map[x - 1][y - 1] = [Cell.Number, 1]
                    pass
            if x - 1 >= 0 and y + 1 < self.y:
                try:
                    self.mine_map[x - 1][y + 1][1] += 1
                except:
                    self.mine_map[x - 1][y + 1] = [Cell.Number, 1]
                    pass

        for loc in self.mine_locations:
            self.mine_map[loc[0]][loc[1]] = Cell.Bomb
