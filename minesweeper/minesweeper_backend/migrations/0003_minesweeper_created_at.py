# Generated by Django 2.2 on 2019-04-19 18:03

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [("minesweeper_backend", "0002_minesweeper_minemap")]

    operations = [
        migrations.AddField(
            model_name="minesweeper",
            name="created_at",
            field=models.DateTimeField(
                auto_now_add=True, default=django.utils.timezone.now
            ),
            preserve_default=False,
        )
    ]
