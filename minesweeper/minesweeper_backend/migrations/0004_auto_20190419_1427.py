# Generated by Django 2.2 on 2019-04-19 18:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("minesweeper_backend", "0003_minesweeper_created_at")]

    operations = [
        migrations.AddField(
            model_name="minesweeper", name="bombs", field=models.IntegerField(null=True)
        ),
        migrations.AddField(
            model_name="minesweeper", name="x", field=models.IntegerField(null=True)
        ),
        migrations.AddField(
            model_name="minesweeper", name="y", field=models.IntegerField(null=True)
        ),
    ]
