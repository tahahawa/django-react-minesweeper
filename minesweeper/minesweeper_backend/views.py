import json

from django.core import serializers
from django.http import HttpResponse, HttpResponseNotAllowed, HttpResponseServerError
from django.shortcuts import render
from django.views import View
from django.views.decorators.csrf import csrf_exempt, get_token

from .models import Minesweeper
# from .serializers import MinesweeperSerializer


from django.core.serializers.json import DjangoJSONEncoder


class SetEncoder(DjangoJSONEncoder):
    def default(self, obj):
        if isinstance(obj, set):
            return list(obj)
        return super().default(obj)


"""
Debug list of all games stored in the db 

from rest_framework import generics

class MinesweeperListCreate(generics.ListCreateAPIView):
    queryset = Minesweeper.objects.all()
    serializer_class = MinesweeperSerializer

"""


"""
The `new_game` method takes a POST request, and creates a new game based on the
parameters passed in (size of x and y, and number of mines), either in JSON
format, or FormData. The primary key is serialized into JSON and returned to
the client. The primary key should then be used to play the game.
"""


@csrf_exempt
def new_game(request):
    """make sure it's a POST, otherwise, it's not allowed"""
    if request.method == "POST":
        """If it's JSON, parse it, otherwise assume that it's FormData"""
        if str(request.META["CONTENT_TYPE"]).startswith("application/json"):
            data = json.loads(request.body)
        else:
            data = request.POST

        """Make sure all the params exist"""
        if "x" in data and "y" in data and "mines" in data:
            """Make sure the params parse into ints correctly"""
            try:
                nx = int(data["x"])
                ny = int(data["y"])
                nm = int(data["mines"])
            except ValueError:
                print(request.POST)
                return HttpResponseServerError("param parse error")

            """Check for data validity"""
            if nx <= 0 or ny <= 0 or nm <= 0:
                return HttpResponseServerError("param value error")

            """Create new game and save it, then send its PK"""
            game = Minesweeper(x=nx, y=ny, mines=nm)
            game.generate()
            game.save()
            return HttpResponse(
                serializers.serialize("json", [game], fields=(), cls=SetEncoder)
            )
        else:
            print(request.POST)
            return HttpResponseServerError("param missing")
    else:
        return HttpResponseNotAllowed(["POST"])


"""
get_game gets all the necessary game info and sends it to the client
"""


def get_game(request, *args, **kwargs):
    """make sure it's a GET, otherwise, it's not allowed"""
    if request.method == "GET":
        pk = int(kwargs["pk"])
        game = Minesweeper.objects.get(pk=pk)
        return HttpResponse(
            serializers.serialize(
                "json",
                [game],
                fields=(
                    "x",
                    "y",
                    "mines",
                    "complete",
                    "win",
                    "revealed",
                    "marked_locations",
                ),
                cls=SetEncoder,
            )
        )
    else:
        return HttpResponseNotAllowed(["GET"])


"""Receive a post from client to reveal a tile"""


def reveal_tile(request, *args, **kwargs):
    """make sure it's a POST, otherwise, it's not allowed"""
    if request.method == "POST":
        pk = int(kwargs["pk"])
        game = Minesweeper.objects.get(pk=pk)

        """If it's JSON, parse it, otherwise assume that it's FormData"""
        if str(request.META["CONTENT_TYPE"]).startswith("application/json"):
            data = json.loads(request.body)
        else:
            data = request.POST

        """Make sure all the params exist"""
        if "x" in data and "y" in data:
            """Make sure the params parse into ints correctly"""
            try:
                nx = int(data["x"])
                ny = int(data["y"])
            except ValueError:
                print(request.POST)
                return HttpResponseServerError("param parse error")

            game.reveal_tile(nx, ny)
            game.save()
            return HttpResponse(
                serializers.serialize(
                    "json",
                    [game],
                    fields=("complete", "win", "revealed"),
                    cls=SetEncoder,
                )
            )

    else:
        return HttpResponseNotAllowed(["POST"])


"""Mark a tile via POST"""


def toggle_mark_tile(request, *args, **kwargs):
    """make sure it's a POST, otherwise, it's not allowed"""
    if request.method == "POST":
        pk = int(kwargs["pk"])
        game = Minesweeper.objects.get(pk=pk)

        """If it's JSON, parse it, otherwise assume that it's FormData"""
        if str(request.META["CONTENT_TYPE"]).startswith("application/json"):
            data = json.loads(request.body)
        else:
            data = request.POST

        """Make sure all the params exist"""
        if "x" in data and "y" in data:
            """Make sure the params parse into ints correctly"""
            try:
                nx = int(data["x"])
                ny = int(data["y"])
            except ValueError:
                print(request.POST)
                return HttpResponseServerError("param parse error")

            game.toggle_mark_tile(nx, ny)
            game.save()
            return HttpResponse(
                serializers.serialize(
                    "json",
                    [game],
                    fields=("complete", "win", "marked_locations", "revealed"),
                    cls=SetEncoder,
                )
            )

    else:
        return HttpResponseNotAllowed(["POST"])
