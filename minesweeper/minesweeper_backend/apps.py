from django.apps import AppConfig


class BackendConfig(AppConfig):
    name = "minesweeper_backend"
