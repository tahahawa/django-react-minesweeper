import React from "react";

class Square extends React.Component {
  val() {
    if (!this.props.value.revealed) {
      if (this.props.value.marked) {
        return "F";
      } else {
        return null;
      }
    }
    if (this.props.value.bomb) {
      return "B";
    }
    if (this.props.value.number === 0) {
      return null;
    }

    return this.props.value.number;
  }

  render() {
    let className = "square";
    if (!this.props.value.revealed) {
      className += " unrevealed";
    }
    if (this.props.value.bomb) {
      className += " bomb";
    }
    if (this.props.value.marked) {
      className += " marked";
    }
    if (this.props.value.number > 0) {
      className += " numbered";
    }

    return (
      <button
        ref="square"
        onClick={this.props.handleClick}
        className={className}
        onContextMenu={this.props.handleRightClick}
      >
        {this.val()}
      </button>
    );
  }
}

export default Square;
