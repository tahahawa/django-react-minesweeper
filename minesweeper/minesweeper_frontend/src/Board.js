import React from "react";
import Square from "./Square";

class Board extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      board: Array
    };
  }
  componentDidMount() {
    // console.log(this.props);
    this.setState({
      board: this.generateBoard(
        this.props.x,
        this.props.y,
        this.props.revealed,
        this.props.marked_locations
      )
    });
  }

  componentDidUpdate(prevProps) {
    if (this.props !== prevProps) {
      this.setState({
        board: this.generateBoard(
          this.props.x,
          this.props.y,
          this.props.revealed,
          this.props.marked_locations
        )
      });
    }
  }

  generateBoard(x, y, revealed, marked_locations) {
    let b = [];
    for (let i = 0; i < x; i++) {
      b.push([]);
      for (let j = 0; j < y; j++) {
        b[i][j] = {
          x: i,
          y: j,
          revealed: false,
          empty: false,
          marked: false,
          number: 0,
          bomb: false
        };
      }
    }
    for (let i = 0; i < revealed.length; i++) {
      let val = revealed[i];
      console.log(val);
      if (val[2] === 0) {
        b[val[0]][val[1]] = {
          x: val[0],
          y: val[1],
          revealed: true,
          marked: false,
          empty: true,
          bomb: false,
          number: 0
        };
      } else if (val[2] === 1) {
        b[val[0]][val[1]] = {
          x: val[0],
          y: val[1],
          revealed: true,
          marked: false,
          empty: false,
          bomb: true,
          number: 0
        };
      } else {
        b[val[0]][val[1]] = {
          x: val[0],
          y: val[1],
          revealed: true,
          marked: false,
          empty: false,
          bomb: false,
          number: val[2][1]
        };
      }
    }
    for (let i = 0; i < marked_locations.length; i++) {
      let val = marked_locations[i];
      b[val[0]][val[1]].marked = true;
    }
    return b;
  }

  renderBoard(board) {
    if (board.length > 1) {
      return board.map(row => {
        return row.map(cell => {
          return (
            // Cantor pairing function
            <div key={((cell.x + cell.x) * (cell.x + cell.y + 1) + cell.y) / 2}>
              <Square
                handleClick={() =>
                  this.props.handleClick(cell.x, cell.y, cell.revealed)
                }
                handleRightClick={e =>
                  this.props.handleRightClick(e, cell.x, cell.y)
                }
                value={cell}
              />
              {row[row.length - 1] === cell ? (
                <div className="board-row" />
              ) : (
                ""
              )}
            </div>
          );
        });
      });
    } else {
      return null;
    }
  }

  render() {
    return <div>{this.renderBoard(this.state.board)}</div>;
  }
}

export default Board;
