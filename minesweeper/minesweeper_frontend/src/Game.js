import React from "react";
import axios from "axios";
import "./Game.css";
import Board from "./Board";

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      x: 0,
      y: 0,
      mines: 0,
      complete: false,
      win: false,
      revealed: [],
      marked_locations: [],
      pk: 0
    };

    this.handleClick = this.handleClick.bind(this);
    this.handleRightClick = this.handleRightClick.bind(this);
  }

  componentDidMount() {
    axios.defaults.xsrfCookieName = "csrftoken";
    axios.defaults.xsrfHeaderName = "X-CSRFToken";
    axios.defaults.withCredentials = true;

    axios
      .get("/api/get_game/" + this.props.match.params.id)
      .then(response => {
        this.setState({
          x: response.data[0].fields.x,
          y: response.data[0].fields.y,
          mines: response.data[0].fields.mines,
          complete: response.data[0].fields.complete,
          win: response.data[0].fields.win,
          revealed: response.data[0].fields.revealed,
          marked_locations: response.data[0].fields.marked_locations,
          pk: this.props.match.params.id
        });
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  handleClick(x, y, revealed) {
    if (!this.state.complete && !revealed) {
      axios.defaults.xsrfCookieName = "csrftoken";
      axios.defaults.xsrfHeaderName = "X-CSRFToken";
      axios.defaults.withCredentials = true;

      axios
        .post("/api/reveal_tile/" + this.state.pk, { x: x, y: y })
        .then(response => {
          this.setState({
            complete: response.data[0].fields.complete,
            win: response.data[0].fields.win,
            revealed: response.data[0].fields.revealed
          });
        })
        .catch(function(error) {
          console.log(error);
        });
    }
  }

  handleRightClick(e, x, y) {
    e.preventDefault();

    if (!this.state.complete) {
      axios.defaults.xsrfCookieName = "csrftoken";
      axios.defaults.xsrfHeaderName = "X-CSRFToken";
      axios.defaults.withCredentials = true;

      axios
        .post("/api/toggle_mark_tile/" + this.state.pk, { x: x, y: y })
        .then(response => {
          this.setState({
            complete: response.data[0].fields.complete,
            win: response.data[0].fields.win,
            marked_locations: response.data[0].fields.marked_locations,
            revealed: response.data[0].fields.revealed
          });
          console.log(this.state);
        })
        .catch(function(error) {
          console.log(error);
        });
    }
  }

  render() {
    let { x, y, revealed, marked_locations } = this.state;
    var ongoing = String(!this.state.complete);

    return (
      <div className="game">
        <div className="game-board">
          <Board
            x={x}
            y={y}
            revealed={revealed}
            marked_locations={marked_locations}
            handleClick={this.handleClick}
            handleRightClick={this.handleRightClick}
          />
        </div>
        <div className="game-info">
          <p>Ongoing: {ongoing}</p>
          <p>Win: {String(this.state.win)}</p>
          <p>Marks used: {String(this.state.marked_locations.length)}</p>
          <p>Mines: {String(this.state.mines)}</p>
        </div>
      </div>
    );
  }
}
export default Game;
