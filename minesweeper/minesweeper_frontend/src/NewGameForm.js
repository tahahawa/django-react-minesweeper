import React from "react";
import axios from "axios";
class NewGameForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    const data = new FormData(event.target);

    axios.defaults.xsrfCookieName = "csrftoken";
    axios.defaults.xsrfHeaderName = "X-CSRFToken";
    axios.defaults.withCredentials = true;

    axios
      .post("/api/new_game", data)
      .then(function(response) {
        window.location = "/game/" + response.data[0].pk;
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          X Size:
          <input
            id="x"
            name="x"
            type="text"
            pattern="[0-9]*"
            value={this.state.x}
            required
          />
        </label>
        <label>
          Y Size:
          <input
            id="y"
            name="y"
            type="text"
            pattern="[0-9]*"
            value={this.state.y}
            required
          />
        </label>
        <label>
          Number of Mines:
          <input
            id="mines"
            name="mines"
            type="text"
            pattern="[0-9]*"
            value={this.state.mines}
            required
          />
        </label>
        <input type="submit" value="Submit" />
      </form>
    );
  }
}
export default NewGameForm;
