import React, { Component } from "react";
import "./NewGameForm";
import "./App.css";
import NewGameForm from "./NewGameForm";

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <p>Minesweeper</p>
          <NewGameForm />
          <p> Left click to reveal tile, right click to flag/unflag tile</p>
        </header>
      </div>
    );
  }
}

export default App;
