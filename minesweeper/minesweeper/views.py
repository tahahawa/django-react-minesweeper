import logging
import os

from django.views.generic import View
from django.http import HttpResponse
from django.conf import settings
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt, ensure_csrf_cookie


class FrontendAppView(View):
    """
    Serves the compiled frontend entry point (only works if you have run `yarn
    run build`).
    """

    @method_decorator(ensure_csrf_cookie)
    def get(self, request):
        try:
            with open(os.path.join(settings.REACT_APP_DIR, "build", "index.html")) as f:
                return HttpResponse(f.read())
        except FileNotFoundError:
            logging.exception("Production build of app not found")
            return HttpResponse(
                """
                run `yarn run build` on frontend
                """,
                status=501,
            )
