Install dependencies for python:

`pip install -r requirements.txt `

Run `yarn install` and then `yarn build` in `./minesweeper/minesweeper_frontend/`

Make sure to set up the DB:

` python minesweeper/manage.py migrate`

Run the django server, and voila!

`python minesweeper/manage.py runserver`